#!/bin/bash

(trap 'kill 0' SIGINT; 
python main.py --model de_simple --port 50051 & 
python main.py --model de_distmult --port 50052 & 
python main.py --model de_transe --port 50053)