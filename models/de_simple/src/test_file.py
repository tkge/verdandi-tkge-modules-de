import torch
import numpy
from dataset import Dataset

dataset = Dataset("icews14")

model_path = "models/DE_SimplE/icews14/500_512_0.001_0.0_68_500_0.4_32_20_0.68_500.chkpnt"

model = torch.load(model_path, map_location=torch.device("cpu")).module.cpu()

# Fact with missing tail
head = torch.IntTensor([0])
rel = torch.IntTensor([0])
year = torch.Tensor([2014.0])
month = torch.Tensor([5.0])
day = torch.Tensor([13.0])

heads = head.repeat(dataset.numEnt(), )
rels = rel.repeat(dataset.numEnt(), )
tails = torch.IntTensor(list(range(dataset.numEnt())))
years = year.repeat(dataset.numEnt(), )
months = month.repeat(dataset.numEnt(), )
days = day.repeat(dataset.numEnt(), )

scores = model(heads, rels, tails, years, months, days)
top_k = torch.topk(scores, 5)
print(top_k)

scripted_model = torch.jit.script(model, example_inputs=[{model: [(heads, rels, tails, years, months, days)]}])
