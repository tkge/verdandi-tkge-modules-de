import logging
import logging.handlers
import logging.config
from argparse import ArgumentParser

from src import serve
from src.python.config import Config
from src.python.servicer import LinkPredictionServicer
from src.python.link_predictor import DESimplEPredictor


def _generate_arg_parser() -> ArgumentParser:
    """Generate an argument parser, that allows for overriding config file on CLI"""
    parser = ArgumentParser(description="Run a TKGE server")
    parser.add_argument("--host", type=str, dest="server.host", help="Server host.")
    parser.add_argument("--port", type=str, dest="server.port", help="Server port.")
    parser.add_argument(
        "--max-workers",
        dest="server.max_workers",
        type=str,
        help="Max workers for server.",
    )
    parser.add_argument(
        "--src", type=str, dest="de_predictor.src", help="Src to add to path."
    )
    parser.add_argument(
        "--model", type=str, dest="etc.model", help="Model to use for de-predictor."
    )
    return parser


if __name__ == "__main__":
    # Parse CLI arguments (if any)
    parser = _generate_arg_parser()
    args = parser.parse_args()

    # Load configurations
    logging.config.fileConfig("logging.conf")
    config = Config(**vars(args))

    # Create objects for dependency injection
    link_predictor = DESimplEPredictor(config)
    servicer = LinkPredictionServicer(link_predictor, config)

    # Start server, inject servicer and config.
    serve(servicer, config)
