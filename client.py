import grpc

import src.python.generated.link_prediction_pb2 as pb2
import src.python.generated.link_prediction_pb2_grpc as pb2_grpc


def client() -> None:
    channel = grpc.insecure_channel("localhost:50052")
    stub = pb2_grpc.LinkPredictionStub(channel=channel)
    query = pb2.LinkPredictionQuery(
        head=0, relation=0, tail=-1, time="13-05-2014", num_replies=5, dataset="icews14"
    )
    response = stub.PredictLink(query)
    print(response)


if __name__ == "__main__":
    client()
