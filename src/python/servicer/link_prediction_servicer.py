import grpc

from ..config import Config
from ..link_predictor import LinkPredictor
from ..generated import link_prediction_pb2 as pb2
from ..generated import link_prediction_pb2_grpc as pb2_grpc


class LinkPredictionServicer(pb2_grpc.LinkPredictionServicer):
    """
    Implementation of generated LinkPredictionServicer.
    Instantiate this, and register it using gRPC.
    When gRPC API `PredictLink` is called, this class' `PredictLink` method is used.
    """

    def __init__(self, link_predictor: LinkPredictor) -> None:
        """Create instance of LinkPredictionServicer.

        Args:
            link_predictor (LinkPredictor): LinkPredictor to use for link prediction, when `PredictLink` is called.
        """
        super().__init__()
        self.link_predictor = link_predictor

    def PredictLink(
        self, request: pb2.LinkPredictionQuery, context: grpc.ServicerContext
    ) -> pb2.LinkPredictionResponse:
        """Use self.link_predictor to process request, and return response."""
        return self.link_predictor.predict_link(request, context)
