from .link_prediction_servicer import LinkPredictionServicer

__all__ = ["LinkPredictionServicer"]
