import sys
import grpc
from datetime import datetime
from pathlib import Path
from typing import List, Optional
import torch
import yaml
import logging

import src.python.generated.link_prediction_pb2 as pb2
from ...config import Config
from .. import LinkPredictionResponse, LinkPredictor

_LOGGER = logging.getLogger()


class DESimplEPredictor(LinkPredictor):
    def __init__(self, config: Config) -> None:
        super().__init__()
        self.config = config
        self.src = config.get("de_predictor", "src")
        self.model = config.get("etc", "model")

    def predict_link(
        self, request: pb2.LinkPredictionRequest, context: grpc.ServicerContext
    ) -> pb2.LinkPredictionResponse:
        _LOGGER.info("Request recieved...")
        _LOGGER.debug(f"Request: {request}")
        head, tail = request.head, request.tail

        # Check if we are missing head or tail. If relation is missing, error.
        if request.relation == -1:
            _LOGGER.warning("Recieved request with missing relation.")
            msg = "Only link prediction with missing head or tail allowed."
            context.set_details(msg)
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            return pb2.LinkPredictionResponse()
        elif head == -1:
            _LOGGER.debug("Request missing head, setting to None.")
            head = None
        else:
            _LOGGER.debug("Request missing tail, setting to None.")
            tail = None

        # Parse time
        try:
            _LOGGER.debug("Parsing time...")
            dt = datetime.strptime(request.time, "%d-%m-%Y")
            year = dt.year
            month = dt.month
            day = dt.day
        except Exception as e:
            _LOGGER.warning("Time could not be parsed, incorrect format.")
            _LOGGER.warning(f"Exception raised: {e}")
            msg = "Date format incorrect, should be dd-mm-YYYY."
            context.set_details(msg)
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            return pb2.LinkPredictionResponse()

        # Compute results of link prediction query, using underlying TKGE.
        result = self._predict_link(
            head,
            request.relation,
            tail,
            year,
            month,
            day,
            request.num_replies,
            request.dataset,
        )

        # Convert and return results
        _LOGGER.info("Returning result...")
        return pb2.LinkPredictionResponse(facts=[lpr.to_grpc_fact() for lpr in result])

    def _predict_link(
        self,
        head: Optional[int],
        relation: int,
        tail: Optional[int],
        year: int,
        month: int,
        day: int,
        num_replies: int,
        dataset: str,
    ) -> List[LinkPredictionResponse]:
        _LOGGER.debug(f"Appending {self.src} to python path")
        # Add source code of used TKGE model to path.
        # This might not be necessary, if torch model is saved and loaded differently.
        sys.path.append(self.src)

        # Load model
        model_path = Path(f"models/{self.model}/{dataset}.chkpnt")
        _LOGGER.debug(f"I am {self.config.get('etc', 'model')}.")
        _LOGGER.debug(f"Loading model with path: {model_path}")
        try:
            model = torch.load(
                model_path, map_location=torch.device("cpu")
            ).module.cpu()  # TODO: Maybe not force CPU?
        except IOError as e:
            # TODO: Handle this better.
            _LOGGER.error(f"Unable to load model. Failed with exception: {e}")

        # Load dataset
        dataset_path = Path(f"data/{dataset}/{dataset}.yaml")
        _LOGGER.debug(f"Loading dataset file with path: {dataset_path}")
        try:
            with open(dataset_path, "r") as dataset_file:
                loaded_dataset = yaml.safe_load(dataset_file)
        except IOError as e:
            # TODO: Handle this better:
            _LOGGER.error(f"Unable to load dataset file. Failed with exception: {e}")

        # Create tensors
        _LOGGER.debug("Creating tensors...")
        num_ent = loaded_dataset["NUM_ENT"]

        relations = torch.IntTensor([relation]).repeat(num_ent)
        years = torch.IntTensor([year]).repeat(num_ent)
        months = torch.IntTensor([month]).repeat(num_ent)
        days = torch.IntTensor([day]).repeat(num_ent)

        heads = (
            torch.IntTensor(list(range(num_ent)))
            if head is None
            else torch.IntTensor([head]).repeat(num_ent)
        )
        tails = (
            torch.IntTensor(list(range(num_ent)))
            if tail is None
            else torch.IntTensor([tail]).repeat(num_ent)
        )

        # Calculate scores
        _LOGGER.debug("Calculating scores...")
        model.eval()  # Disable dropout
        scores = model(heads, relations, tails, years, months, days)
        top_k_scores = torch.topk(scores, num_replies)

        # Create return value
        rv = []
        for i in range(num_replies):
            rhead = head if head is not None else top_k_scores.indices.numpy()[i]
            rtail = tail if tail is not None else top_k_scores.indices.numpy()[i]
            rv.append(
                LinkPredictionResponse(
                    rhead,
                    relation,
                    rtail,
                    year,
                    month,
                    day,
                    top_k_scores.values.data.numpy()[i],
                )
            )

        _LOGGER.debug("Returning top-k facts...")
        _LOGGER.debug(f"top-k facts: {rv}")
        return rv
