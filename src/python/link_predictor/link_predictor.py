from abc import ABC, abstractmethod
from dataclasses import dataclass
from datetime import datetime
from typing import List
import grpc

from ..generated import link_prediction_pb2 as pb2


@dataclass
class LinkPredictionResponse:
    """
    Helper dataclass for storing temporal KG facts with their scores.
    Also contains logic for converting from this to generated `LinkPredictionResponse`.
    """

    head: int
    relation: int
    tail: int
    year: int
    month: int
    day: int
    score: float

    def to_grpc_fact(self) -> pb2.Fact:
        dt = datetime(self.year, self.month, self.day)
        return pb2.Fact(
            head=self.head,
            relation=self.relation,
            tail=self.tail,
            time=dt.strftime("%d-%m-%Y"),
            score=self.score,
        )


class LinkPredictor(ABC):
    @abstractmethod
    def predict_link(
        self, request: pb2.LinkPredictionQuery, context: grpc.Context
    ) -> pb2.LinkPredictionResponse:
        """Convert `request` to a temporal link prediction query, solve, and return results.

        Args:
            request (pb2.LinkPredictionQuery): Generated class containing head, relation, tail, time and number of responses to return.
            context (grpc.Context): Context for use with gRPC.

        Returns:
            pb2.LinkPredictionResponse: Generated class containing n (where n comes from `request`) temporal KG facts.
        """
        raise NotImplementedError
