from configparser import ConfigParser
import logging
from typing import Optional, List
from pattern_singleton import Singleton  # type: ignore

_LOGGER = logging.getLogger()


class Config(metaclass=Singleton):
    """
    Class for accessing configuration items. Loads items from `config.ini`.
    Singleton class, so only one instance can exist.
    """

    def __init__(
        self,
        config_path: str = "config.ini",
        **kwargs: str,
    ) -> None:
        _LOGGER.info("Loading configuration...")
        self._config = ConfigParser()
        self._config.read(config_path)
        _LOGGER.debug("Loaded configuration file...")

        # If arguments passed to constructor, those override what is in config file.
        _LOGGER.debug("Overriding configuration with CLI-arguments, if provided...")
        for item in kwargs.items():
            if item[1] is None:
                continue
            # Expect keys as 'section.option'
            section = item[0].split(".")[0]
            option = item[0].split(".")[1]
            self._config.set(section, option, item[1])
            _LOGGER.debug(f"{item[0]} overrided with {item[1]}")

    def get(self, section: str, option: str) -> Optional[str]:
        return (
            self._config.get(section=section, option=option)
            if self._does_exist(section, option)
            else None
        )

    def getboolean(self, section: str, option: str) -> Optional[bool]:
        return (
            self._config.getboolean(section=section, option=option)
            if self._does_exist(section, option)
            else None
        )

    def getint(self, section: str, option: str) -> Optional[int]:
        return (
            self._config.getint(section=section, option=option)
            if self._does_exist(section, option)
            else None
        )

    def getfloat(self, section: str, option: str) -> Optional[float]:
        return (
            self._config.getfloat(section=section, option=option)
            if self._does_exist(section, option)
            else None
        )

    def sections(self) -> List[str]:
        return self._config.sections()

    def options(self, section: str) -> List[str]:
        return self._config.options(section)

    def _does_exist(self, section: str, option: str) -> bool:
        if not self._config.has_section(section):
            return False
        if not self._config.has_option(section, option):
            return False
        return True

    def _set(self, section: str, option: str, value: str) -> None:
        if not self._config.has_section(section):
            self._config.add_section(section)
        self._config.set(section, option, value)
