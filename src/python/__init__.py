from .servicer import LinkPredictionServicer
from .server import serve

__all__ = ["serve", "LinkPredictionServicer"]
