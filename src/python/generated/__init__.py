import src.python.generated.link_prediction_pb2 as link_prediction_pb2
import src.python.generated.link_prediction_pb2_grpc as link_prediction_pb2_grpc

__all__ = ["link_prediction_pb2", "link_prediction_pb2_grpc"]
