# Temporal Knowledge Graph Embedding Module

TKGE modules of `Verðandi`

## Installation

Install requirements using [poetry](https://python-poetry.org/).

```bash
poetry install
```

or [pip](https://pip.pypa.io/en/stable/)

```bash
pip install -r requirements.txt
```

## Usage

Use as-is, by entering the wanted host, port, and number of workers into `config.ini`.
Model can also be changed under `[etc]` of configuration file. Currently, `de_simple`, `de_distmult` and `de_transe` are accepted options.

Run using `main.py`. Should be used in tandem with [Verðandi UI and NL modules](https://gitlab.com/tkge/verhandi-ui-nl), and optionally [Verðandi Ensemble module](https://gitlab.com/tkge/verhandi-tkge-module-ensemble/-/blob/main/README.md).

## Development

To include a new model, create a new implementation of `LinkPredictor`, where method `predict_link` takes generated class `LinkPredictionQuery` and returns generated class `LinkPredictionResponse`.

See `de_simple_predictor.py` for inspiration.


## License

[MIT](https://choosealicense.com/licenses/mit/)
